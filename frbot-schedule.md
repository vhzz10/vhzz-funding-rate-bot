# Funding Rate Bot

- This bot for placing order to binance exchange at 59:56 every 8 hours and closing by TP/SL from your config
- This package has code to place order and scheduler config from env, if you need more feature like config page, order dashboard, schedule jobs page, ... please see other packages [here]()


## Purchase source [here]()


## Prepare

- [ ] OS: ubuntu, macos
- [ ] python 3.9.11


## Install requirements by pip

```
    pip install -r requirements.txt
```


## OR Install requirements by pipenv

```
    pipenv install
```


## Config with env

```
    copy .env-template to .env
    Edit .env follow template that I inputted
    
    *NOTE
        CRON_HOUR is funding time of binance - 1
        CRON_MINUTE should be 59, it will running and waiting until 59:56 and start to check market then place order
        
```

## Run

```
    PYTHONPATH=`pwd` pipenv run python ./cron/fr_bot.py
```

***

## TIPS

- [ ] Should run from cloud server to no delay from binance

## NOTE

- [ ] You must allow future trade on API management from binance
