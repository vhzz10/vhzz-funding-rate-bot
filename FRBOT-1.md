# Funding Rate Bot



## Purchase source [here]()


## Prepare

- [ ] python 3.9.11


## Install requirements by pip

```
    pip install -r requirements.txt
```


## Install requirements by pipenv

```
    pipenv install
```

## Run

```
    /bin/bash ./bin/run.sh
```

***

## TIPS

- [ ] You can set `run.sh` into crontab
- [ ] Should run from cloud server to no delay from binance

## NOTE

- [ ] You must allow future trade on API management from binance
